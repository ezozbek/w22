import React from 'react';
import Component from './Component';

const App = () => {
  return (
    <div>
      <h1>My App</h1>
      <Component country="Finland" />
      <Component country="Norway" />
    </div>
  );
};

export default App;
